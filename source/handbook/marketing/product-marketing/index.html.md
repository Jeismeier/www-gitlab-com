---
layout: markdown_page
title: "Product Marketing"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Roles

- **PM** - Product Management or Product Manger. The product team as a whole, or the specific person responsible for a product area.
- **PMM** - Product Marketing Management or Product Marketing Manager. The product marketing team as a whole, or the specific person responsible for a product marketing area.

## What is PMM working on?
- View the [Product Marketing Issue Board](https://gitlab.com/gitlab-com/marketing/boards/397296?=&label_name[]=Product%20Marketing) to see what's currently in progress.
- To ask for PMM resources log an issue in the marketing project and label it with `Product Marketing`.
- The PMM team does sprint planning on a biweekly basis where the backlog is reviewed and issues are prioritized.
- If you need more immediate attention please send a message in the `#product-marketing` slack channel.

## Release vs Launch
A [product release, and a marketing launch are two separate activities](http://www.startuplessonslearned.com/2009/03/dont-launch.html). The canonical example of this is Apple. They launch the iPhone at their yearly event and then releases it months later. At GitLab we do it the other way: Release features as soon as they are ready letting customers use them right away, and then, do a marketing launch later when we have market validation.

| Release | Launch |
|-|-|
| PM Led | PMM Led |
| New features can ship with or without marketing support | Launch timing need not be tied to the proximity of when a feature was released |

## 9 types of users

There are 9 types of GitLab users:

1. Libre
1. Trial
1. Starter
1. Premium
1. Ultimate
1. Free
1. Bronze
1. Silver
1. Gold

Groupings of the types of users are:

- **Open source** (Libre and Free) vs. **Enterprise** (Trial, Starter, Premium, Ultimate, Bronze, Silver, and Gold)
- **Gratis** (Libre, Trial, and Free) vs. **Customers** (Starter, Premium, Ultimate, Bronze, Silver, and Gold)
- **Self-hosted** (Libre, Trial, Starter, Premium, Ultimate) vs. **GitLab.com** (Free, Bronze, Silver, Gold)

In talking about what type of subscription you need for a feature we use the self-hosted tiers (Libre, Starter, Premium, Ultimate) since self-hosted features are a superset of GitLab.com features.

GitLab.com subscriptions are added to either a personal namespace or a group namespace.

Libre users can use either of our two distributions: Community Edition (CE) and Enterprise Edition (EE). EE without a license has the same functionality as CE. CE vs. EE refers solely to the distribution, **never use CE vs. EE as a substitute for non-paying users vs. customers.** By grouping our non-open-source users as Enterprise we muddle the waters a bit, we do this to make the transition easier. Please know that GitLab Enterprise is not a distribution.

Gratis users are [free as in speech and as in beer](http://www.howtogeek.com/howto/31717/what-do-the-phrases-free-speech-vs.-free-beer-really-mean/). Even when they use the EE distribution, the code paths that are executed are all licensed under an open source license.

There is only one type of trial: self-hosted. These users get all functionality in GitLab Ultimate. We do not offer a trial for GitLab.com at this moment.

We can stop referring to self-hosted plans as `Enterprise Edition Starter`, `Enterprise Edition Premium`, and `Enterprise Edition Ultimate`. This also applies to the apprviations `EES`, `EEP`, and `EEU`. Use the names `Starter`, `Premium`, and `Ultimate` to refer to our customer self-hosted plans, never something like 'Enterprise Starter'.

If you wonder about the meaning of libre, gratis, and free please have a look at [the wikipedia article about free software](https://en.wikipedia.org/wiki/Free_software).

## Competitive Intelligence

- Post any competitive info in the `#competition` slack channel.
- Link to the blog post, announcement, or feature page your are referencing.
- If you received the intel from a customer conversation, link to the Salesforce record and the full set of notes from the conversation.

## Customer Stories
A formal case study produces a polished, finished document. A "customer story" is a shorter, informal story about how a customer solved a particular problem or received a specific benefit. Case studies are usually made up of multiple customer stories for a single customer. Customer stories can also be anonymous. e.g. "A large international financial services company used Gitlab CI/CD to reduce build times from 1 hour to 10 mins."

Today, we don't have a central repository for customer service, but this is a Q1 goal to start building one. For now we are capturing customer stories in an issue. If you have a customer story or anecdote [leave a comment on issue 1834](https://gitlab.com/gitlab-com/marketing/issues/1834).

## Customer Case Study Creation

### Objectives:
- Illustrate successful and measurable improvements in the customer’s business
- Show other users how customers are using GitLab
- Align content so that GitLab solutions reflect the challenges our customers, prospects, and the market requirements
- Develop and grow organizational relationships: speak at events on our behalf, promote their business
- Support our value proposition - Faster from planning to monitoring

### Creating the Customer Case Study:

#### Kicking off a new case study
To start a new case study process, [create a new issue in the Marketing issue tracker](https://gitlab.com/gitlab-com/marketing/issues/new?issue) and use the "Customer_Story_Kickoff" template.

**Explaining the Case Study process to a Customer & Creating the Case Study**:

Below is an example email that PMM may send customers to after the AE has introduced them to the customer. The email below will help the customer to get a better sense of what we are asking of them.

>Hi X,

>It's nice to e-meet you, we'd love to hear more about your journey to GitLab and potentially write a customer story on COMPANY NAME.

>Here are the steps that we'd work with you on.
>- 20-30 minute phone call to hear more about your industry, business, and team. (In the call, we would also like to hear more about your decision making process to first choose GitLab and then eventually purchase EE.)
>- Review of the draft customer story
>- Final review of the customer story Then once the customer story is agreed upon by you or someone on your team we will publish it on GitLab's channels.

>Please let me know if you're open to kicking off the customer story process on X date

### Collecting Metrics:
Possible quantitative metrics that the customer can be asked to share with GitLab include:
- Reduced cycle time
- Number of deploys in a given time frame
- Reduced number of bugs or Reverts
- Reduced number of admin hours
- Cost savings % through purchasing GitLab: Reduce the cost of managing a number of different people, projects, and platforms.
- Reduction in internal support tickets requests: Reduction in the number of support tickets team submitting to fix challenges, compared to initial SCM tool

The customer case study should then be written by Product Marketing team, and the draft sent to the customer for their input, and approval.

Other sections in the case study can be found on the customer's website or by searching the web - Sidebar summary, The Customer.

Following approval from the customer, the Design team should be sent a doc of the case study to include in the case study design template. The case study can then be published on our website.

### Case Study Format:

*Headline:* The name of the customer and the benefit they gained from implementing our solution

*Sidebar Summary:* Summarize key points
- Customer Name and Logo
- Customer Details: Country, Website
- Organization Type - Public/Private & Industry
- Annual Revenue - If publicly available
- Employees
- Summary of Key Benefits

*The Customer:* A brief introduction of the customer, not the solution.

*The Challenge:* What was the customer trying to change or improve. What regulations or market conditions drove the customer to search for a new solution. Share the steps the customer took to solve the problem, including other products and services they investigated.

*The Solution:* Detail the solution and how it aligns to the customers requirements. Also detail other solutions that GitLab interfaces with. Also include customer quote.

*The Results:* Detail how GitLab supported the customer to solve their problems. This is where the use of benchmarking metrics such as such as time saved, reduced costs, increased performance etc. are required.

*About GitLab:* Short paragraph on GitLab - about, solutions etc. Call to action of solutions offered.

*Possible Additional Supporting Documents:*
- Customer Deal Summary – High Level PowerPoint summary after deal first signed.
- Customer Success Overview
- Infographic – Single page A4 summary with diagrams and measurable benchmarks
- Benchmark Metrics
- Publish on website
- Video - Short video summary if customer is willing to participate - Perforce example
- Blog Post - Blog post to launch customer case study
- Keywords for SEO etc.

### Creating Case Study Blog Post:
Refer to the [guide available here](/handbook/marketing/marketing-sales-development/content/#casestudyblogposts).

## Partner Marketing Objectives

- Promote existing partnerships to be at top-of-mind for developers.
- Integrate resale partnerships: promote partnership integrations/products which we sell as part of our sales process.
- Migrate open source projects to adopt GitLab, and convert their users in-turn to GitLab.
- Surface the ease of GitLab integration to encourage more companies to integrate with GitLab.
- Build closer relationships with existing partners through consistent communication

For a list of Strategic Partners, search for "Partnerships" on the Google Drive.

## Partner Marketing Activation

Our partner activation framework consists of a series of action items within a high-level issue. When a strategic partnership is signed, Product Marketing will choose the issue template called [partner-activation](https://gitlab.com/gitlab-com/marketing/issues/new) which will trigger notification for all involved in partner activation activities.

For each action item within this issue, a separate issue should be created by the assignee who is responsible for that action item within the allocated timeframe.

The partner should be included on the high level issue so they can see the planned activities and can contribute.

## Partner Newsletter Feature

In line with the objective of: Promote existing partnerships to be at top-of-mind for developers, a regular feature in our fortnightly (8th & 22nd) newsletter will promote our partners to our target audience. This feature should be co-authored by the partner.

Possible content:

- Feature on new partner if signed
- Feature on existing partner if major update released
- Feature on existing partner highlighting benefits of partner product
- 1-minute video showcasing the integration as a reference
- Blog post from partner
- Feature on how existing customer uses GitLab and partner

Suggested Format:

- Length: Couple of paragraphs
- Links: GitLab integration/partner page & partner website

Creation:

Email potential partner with case for creating content/blog post which will feature in our newsletter. Also request that they include the content in their own newsletter.

Create separate issue for blog post in www-gitlab-com project with blog post label and assign to Content Marketing with the following information:
- A summary describing the partnership
- Any specific goals for the blog post (e.g. must link to this, mention this, do not do this...)
- Contacts from both sides for people involved in the partnership
- Any marketing pages or material created from both sides which we can use in the post
- Deadlines for the post (internal draft, partner draft review and publication)

After Publication: Send to partner a summary of the click-throughs to their website, registration for webinar etc.

## Reseller Marketing Objectives

- Support resellers to be successful and grow their business
- Motivate resellers to reach first for GitLab
- Promote Reseller program to recruit new resellers

## Reseller Funds Allocation Determination<

- Resellers will request event, SWAG or campaign support by creating an issue using the [reseller issue template](https://gitlab.com/gitlab-com/resellers/issues/new). When the reseller has completed the issue template detailing their needs, Product Marketing and the Channel Reseller Director will be notified.
- When a reseller requests funds for online marketing campaigns, let them know that we can run the campaign in-house working with the Demand Generation team, and even provide artwork if required. All we need is the redirect links to the: /gitlab.com on their website.
- Post-event or campaign, set up a catch-up call with the reseller to determine ROI of GitLab investment.
- At the post event catch-up with the reseller, there are two tabs in the "GitLab events sponsorship request form (Responses)" sheet found on the google drive to be updated - one for events and the other online marketing campaigns.
- Request that the reseller send photos of the event, write a couple of short paragraphs on their experience at the event, any highlights, and impact on their business and GitLabs, and send to you. Photos and a short blog post could feature in the Reseller newsletter, the company-wide newsletter.
- After the Online Marketing campaign, we will send the reseller a summary from Google Adwords or equivalent with the metric measurements detailing the success of the campaign. Store campaign summaries in the "Online Marketing Campaign Summaries" folder on the Google Drive.  
- As we gather more feedback, we will be able to assign a ROI to our marketing investments, and drive more SQLs.
