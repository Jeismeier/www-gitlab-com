---
layout: markdown_page
title: "Jobs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Job

For each job at GitLab, there should be one job as it is the single source of truth. A job has the url /roles/title and contains the following paragraphs:

- Overview
- Levels (junior, intermediate, senior, staff, etc.) which describe the requirements for each level
- [Specializations](/roles/specialist) (CI/CS, distributed systems, Gitaly) relevant to that job
- Hiring process
- Apply
- About GitLab
- Compensation

The job does not contain:

- Locations (EMEA, Americas, APEC)
- [Expertises](/roles/expert) since these are free form.

The job description will be used _both_ for the [Vacancy Creation Process](/handbook/hiring/vacancies/#vacancy-creation-process), as well as serving as the requirements that team members and managers alike use in conversations around career development and performance management.

## New Job Creation

If a hiring manager is creating a new job within the organization, the hiring manager will need to create the job. If this is a job that already exists (for example, Gitaly Developer would use the Developer job description), update the current job description to stay DRY. If the compensation for the role is the same as one already in `/jobs`, you should just update the specialty, not create a new job.

1. Create the relevant page in `https://about.gitlab.com/roles/[name-of-job]`, being sure to use only lower case in naming your directory if it doesn't already exist.
1. Add each paragraph to the job description, for an example see the [developer job](/job/developer)
1. Assign the Merge Request to your manager, executive leadership, and finally the CEO to merge. Also, cc `@gl-peopleops` for a compensation review.
1. Once the merge request has been merged, the People Ops Analyst will propose an appropriate compensation benchmark for the role.
1. The People Ops Analyst will send an email of the proposal to the Chief Culture Officer and Executive of the department for benchmark approval.
1. Once approved, the People Ops Analyst will add the benchmark to the `jobs.yml` file which will automatically cause the [Compensation Calculator](/handbook/people-operations/global-compensation) to show at the bottom of the job description page.
