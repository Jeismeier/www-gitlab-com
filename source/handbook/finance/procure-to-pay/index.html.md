--- 
layout: markdown_page
title: "Procure to Pay"
---

## Procure to Pay Process

#### Requirement Identification 
Team members can purchase goods and services on behalf of the company in accordance with the [Signature Authorization Matrix](/handbook/finance/authorization-matrix/) and guide to [Spending Company Money](/handbook/spending-company-money). However, any purchases requiring contracts must first be reviewed by legal then signed off by a member of the executive team. Be sure to also check out our guide on [Signing Legal Documents](/handbook/signing-legal-documents/). 

#### Approval Workflow
Consult the [Signature Authorization Matrix](/handbook/finance/authorization-matrix/) and determine the approvals required. Create a confidential issue in the [Finance Issue Tracker](https://gitlab.com/gitlab-com/finance) and tag the required functional and financial approvers. Once a purchase is approved, the vendor invoice is sent to our AP inbox (ap@gitlab.com). From there, our Senior Accounts Payable Administrator will forward the invoice to the appropriate personnel for review and approval. Explicit approval is required and should be made via email. This allows us to document and file the approval workflow for future reference. The invoice will then be filed in the "Enter Unpaid" folder in Google Drive. 

#### Invoice Recording and Filing 
After the invoice has been approved and filed in Google Drive, the Senior Accounts Payable Administrator will record the invoice in NetSuite and move the document to another folder in Google Drive named "Invoice to Pay."

#### Payment
The final step in the process is payment. Payment runs are made every Friday. The Senior Accounts Payable Administrator will queue up the payments and send to the Controller for review and approval. Once approved, the Controller submits the payments to the CFO for final approval. Payments are then disbursed and the Senior Accounts Payable Administrator transfers the file to a folder in Google Drive named "Vendor Paid." 
This marks the end of the Procure to Pay process.

