---
layout: job_page
title: "Partner Product Marketing Manager"
---

## Requirements

* Technical background or good understanding of developer products; familiarity with Git, Continuous Integration, Containers, and Kubernetes a plus.
* 3-5 years of experience in product marketing, desired experience in the [specialty](#specialties).
* Able to coordinate across many teams and perform in fast-moving startup environment.
* Excellent spoken and written English
* Proven ability to be self-directed and work with minimal supervision.
* Experience with Software-as-a-Service offerings and open core software a plus.
* Outstanding written and verbal communications skills with the ability to explain and translate complex technology concepts into simple and intuitive communications.
* Uses data to measure results and inform decision making and strategy development.
* You share our [values](/handbook/values), and work in accordance with those values.

## Specialties

### Enterprise Specialist

Do you have experience uncovering a product's core benefits, marketing to an enterprise buyer, or navigating a sales process? Product marketers are at the intersection of all teams.
GitLab is looking for a collaborative and process-driven product marketer to help craft our enterprise story, align to our sales process, and deliver effective go-to-market and sustain campaigns.

The Partner Product Marketing Manager role includes 4 key areas of focus:

* Enterprise-focused product messaging
* Enterprise-focused GTM
* Sales enablement
* Market research

#### Responsibilities

Enterprise Marketing:
* Conduct market research to understand buyer personas, competitive landscape, and other data to help influence marketing, sales, and product decisions.
* Own sales enablement materials and training.
* Work with product and engineering to craft the enterprise story for new and existing features.
* Work with sales, demand generation, and content marketing to create content and campaigns that is geared to our enterprise audience.
* Help technical writing and engineering to create documentation that address audience needs.

### Partner and Channel Marketing

As the Partner Product Marketing Manager, you will work with our marketing, business development and sales teams, to develop and execute global go-to-market strategies and programs to drive customer acquisition and revenue growth for GitLab. Reporting to the Director, Product Marketing, you will have a global charter to enable partners and resellers within our growing partner and channel ecosystems. In addition, you’ll be responsible for crafting and implementing co-marketing campaigns targeting developers and IT professionals, to increase awareness and adoption of GitLab. You will work to shape go-to-market messaging and strategy for new offerings with partners and work with partners and resellers to build an effective and scalable ecosystem.

#### Responsibilities

Channel Marketing:
* Develop our worldwide channel marketing strategy, and train the field marketing team on regional execution that maximizes our reach and scale with channel partners.
* Implement channel marketing initiatives, programs, communication vehicles, and sales tools to drive increased market adoption and channel revenue goals.
* Spearhead enablement activity with resellers to drive end-user adoption, and manage MDF program.
* Assist sales team in the development of actionable and measurable programs for our channel partners.
* Work with field marketing to execute channel marketing programs, regionally.
* Accurately track activity performance and provide well-informed recommendations on future resource and budget allocation.
* Assist in the preparation of reports to help evaluate activity and reseller effectiveness, conversion rate, and relative performance.
* Market to, and through our channel. Ensure prospective resellers understand the value of partnering with GitLab.
* Complete other duties as assigned to meet company channel goals.

Partner Marketing:
* Initiate and develop co-marketing initiatives to execute in tandem with our partners.
* Market the benefit of our technology partnerships to all GitLab prospects.
* Develop and execute marketing programs when announcing new technology partnerships.
* Develop sales enablement programs for new partnerships, training the sales team on added functionality and associated benefits.
* Develop an ROI performance analysis for partner marketing initiatives and utilize findings to tailor future initiatives.
* Develop outreach plan for existing partnerships maintaining a steady development of joint content and demand generation activities.
* Create partnership positioning program for a cloud native approach and associated ecosystem.

#### Responsibilities 

* Able to build strong relationships with partners globally.

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Qualified applicants will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Candidates then may be offered a 45 minute interview with our Chief Marketing Officer
* Next, candidates will be invited to schedule a interview with the Senior Director of Marketing and Sales
* Candidates will then be invited to schedule 45 minute interviews with our Regional Sales Director of the US East Coast
* After, candidates may be invited to schedule a 45 minute interview with the Vice President of Product
* Next, candidates will be invited to schedule a interview with the Chief Revenue Officer.
* Finally, our CEO may choose to conduct a final interview.
* Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
